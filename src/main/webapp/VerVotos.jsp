<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Votaci&oacute;n mejor jugador liga ACB</title>
        <link href="estilos.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="ver">
        <h1>Votaci&oacute;n al mejor jugador de la liga ACB</h1>
        <hr>

        <table border="1" id='tabla'>
            <caption>Votos de cada jugador</caption>
            <tr>
            <th scope="col">nombre</td>
            <th scope="col">votos</td>
            </tr>
        <%
            int numeroJugadores = (Integer) session.getAttribute("numeroJugadores");

            String nombre;

            for (int i = 0; i < numeroJugadores; i++) {
                nombre = (String) session.getAttribute("jugador" + i + "Nombre");
                out.println("<tr>");
                out.println("<td>" + nombre + "</td>");
                out.println("<td class='votos' id='" + nombre + "'>" + (Integer) session.getAttribute("jugador" + i + "Votos") + "</td>");
                out.println("</tr>");
            }
        %>
        </table>
        <br>Hay <%=numeroJugadores%> jugadores
        <br>
        <br> <a href="index.html">Ir al comienzo</a>
    </body>
</html>
