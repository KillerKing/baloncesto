
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.ArrayList;
import java.util.List;

public class Ver extends HttpServlet {

    private ModeloDatos bd;

    @Override
    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession s = req.getSession(true);
        
        int number = bd.obtenerNumeroJugadores();

        s.setAttribute("numeroJugadores", number);

        int votos = 0;

        List<String> listaNombres;
        List<Integer> listaVotos;
        
        listaNombres = bd.obtenerNombres();
        listaVotos = bd.obtenerVotos();

        for (int i = 0; i < number; i++) {
            s.setAttribute("jugador" + i + "Nombre", listaNombres.get(i));
            s.setAttribute("jugador" + i + "Votos", listaVotos.get(i));
        }
        
        // Llamada a la página html que nos muestra los votos
        res.sendRedirect(res.encodeRedirectURL("VerVotos.jsp"));
    }

    @Override
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
