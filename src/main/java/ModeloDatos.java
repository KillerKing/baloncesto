import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level; 

public class ModeloDatos {

    private final static Logger LOGGER = Logger.getLogger(ModeloDatos.class.getName());

    private Connection con;
    private Statement set;
    private ResultSet rs;

    public void abrirConexion() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Con variables de entorno
            String dbHost = System.getenv().get("DATABASE_HOST");
            String dbPort = System.getenv().get("DATABASE_PORT");
            String dbName = System.getenv().get("DATABASE_NAME");
            String dbUser = System.getenv().get("DATABASE_USER");
            String dbPass = System.getenv().get("DATABASE_PASS");

            String url = dbHost + ":" + dbPort + "/" + dbName;
            con = DriverManager.getConnection(url, dbUser, dbPass);

        } catch (Exception e) {
            // No se ha conectado
            LOGGER.log(Level.WARNING, "No se ha podido conectar");
            LOGGER.log(Level.WARNING, "El error es: " + e.getMessage());
        }
    }

    public boolean existeJugador(String nombre) {
        boolean existe = false;
        String cad;
        try {
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                cad = rs.getString("Nombre");
                cad = cad.trim();
                if (cad.compareTo(nombre.trim()) == 0) {
                    existe = true;
                }
            }
            rs.close();
            set.close();
        } catch (Exception e) {
            // No lee de la tabla
            LOGGER.log(Level.WARNING, "No lee la tabla");
            LOGGER.log(Level.WARNING, "El error es: " + e.getMessage());
        }
        return (existe);
    }

    public void actualizarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos=votos+1 WHERE nombre " + " LIKE '%" + nombre + "%'");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No modifica la tabla
            LOGGER.log(Level.WARNING, "No modifica la tabla");
            LOGGER.log(Level.WARNING, "El error es: " + e.getMessage());
        }
    }

    public void insertarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("INSERT INTO Jugadores " + " (nombre,votos) VALUES ('" + nombre + "',1)");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No inserta en la tabla
            LOGGER.log(Level.WARNING, "No inserta en la tabla");
            LOGGER.log(Level.WARNING, "El error es: " + e.getMessage());
        }
    }

    public void borrarVotos() {
        String nombre;
        Statement set2 = null;
        try {
            set = con.createStatement();
            set2 = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                nombre = rs.getString("Nombre");
                set2.executeUpdate("UPDATE Jugadores SET votos=0 WHERE nombre " + " LIKE '%" + nombre + "%'");
            }
            rs.close();
            set.close();
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "No modifica la tabla");
            LOGGER.log(Level.WARNING, "El error es: " + e.getMessage());
        } finally {
            try {
                set2.close();
            }
            catch (Exception e) {
                LOGGER.log(Level.WARNING, "No se cierra el statement");
                LOGGER.log(Level.WARNING, "El error es: " + e.getMessage());
            }
        }
    }

    public int obtenerNumeroJugadores() {
        int number = 0;
        try {            
            set = con.createStatement();
            
            rs = set.executeQuery("SELECT COUNT(*) FROM Jugadores");
            
            while (rs.next()) {
                number = rs.getInt(1);
            }
            
            rs.close();
            set.close();
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "No lee de la tabla");
            LOGGER.log(Level.WARNING, "El error es: " + e.getMessage());
        }        
        return number;
    }

    public List<String> obtenerNombres() {
        List<String> listaNombres = new ArrayList<String>();
        try {            
            set = con.createStatement();
            
            rs = set.executeQuery("SELECT * FROM Jugadores ORDER BY Nombre");
            
            while (rs.next()) {
                listaNombres.add(rs.getString("Nombre"));
            }
            
            rs.close();
            set.close();
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "No lee de la tabla");
            LOGGER.log(Level.WARNING, "El error es: " + e.getMessage());
        }        
        return listaNombres;
    }

    public List<Integer> obtenerVotos() {
        List<Integer> listaVotos = new ArrayList<Integer>();
        try {            
            set = con.createStatement();
            
            rs = set.executeQuery("SELECT * FROM Jugadores ORDER BY Nombre");
            
            while (rs.next()) {
                listaVotos.add(rs.getInt("Votos"));
            }
            
            rs.close();
            set.close();
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "No lee de la tabla");
            LOGGER.log(Level.WARNING, "El error es: " + e.getMessage());
        }        
        return listaVotos;
    }

    public void cerrarConexion() {
        try {
            con.close();
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }
    }

}
