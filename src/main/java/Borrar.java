
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Borrar extends HttpServlet {

    private ModeloDatos bd;

    @Override
    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getSession(true);
        
        bd.borrarVotos();
        
        // Llamada a la página html que nos informa del borrado
        res.sendRedirect(res.encodeRedirectURL("borrado.html"));
    }

    @Override
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
