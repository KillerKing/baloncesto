import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.List;

public class PruebasPhantomjsIT {

    private static WebDriver driver=null;

    @Test
    public void tituloIndexTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new
        String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(),
        "El titulo no es correcto");
        System.out.println(driver.getTitle());
        driver.close();
        driver.quit();
    }

    @Test
    public void votosACeroTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new
        String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);

        driver.navigate().to("http://localhost:8080/Baloncesto/");
        
        driver.findElement(By.className("cuarto")).click();
        driver.findElement(By.name("B1")).click();
        driver.findElement(By.linkText("Ir al comienzo")).click();
        driver.findElement(By.name("B3")).click();
        driver.findElement(By.linkText("Ir al comienzo")).click();
        driver.findElement(By.name("B4")).click();

        List<WebElement> e = driver.findElements(By.className("votos"));
        
        for (WebElement we : e) {
            System.out.println(Integer.parseInt(we.getText()));
            assertEquals(0, Integer.parseInt(we.getText()), "Los votos no son cero");
        }

        driver.close();
        driver.quit();
    }

    @Test
    public void nuevoJugadorTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new
        String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);

        driver.navigate().to("http://localhost:8080/Baloncesto/");
        
        driver.findElement(By.id("Otros")).click();
        driver.findElement(By.name("txtOtros")).sendKeys("Juanito");
        driver.findElement(By.name("B1")).click();
        driver.findElement(By.linkText("Ir al comienzo")).click();
        driver.findElement(By.name("B4")).click();

        WebElement e = driver.findElement(By.id("Juanito"));
        System.out.println(Integer.parseInt(e.getText()));
        assertEquals(1, Integer.parseInt(e.getText()), "Los votos no son 1");

        driver.close();
        driver.quit();
    }
}